/*
 *  This sketch sends random data over UDP on a ESP32 device
 *
 */
#include <WiFi.h>
#include <WiFiUdp.h>

// DECLARATIONS
// WiFi Related
const char * networkName = "JBDirtRoader";
const char * networkPswd = "kfct9vf6";
const char * udpAddress = "192.168.4.255";
const int udpPort = 3333;
// Flow Sensor Related
byte sensorInterrupt = 21; // Sensor Interupt Pin
byte sensorPin       = 21; // Sensor Pin
float calibrationFactor = 4.8; // Flow rate calibration value
volatile byte pulseCount;  // Pulse count tracker for interupt calculations
float flowRateRaw;
float flowRate; 
unsigned int flowMilliLitres;
unsigned int flowMilliLitresRaw;
unsigned long totalMilliLitres;
bool resetVolume = false;
unsigned long oldTime;


//Are we currently connected?
boolean connected = false;

//The udp library class
WiFiUDP udp;

void setup(){
  // Initilize hardware serial:
  Serial.begin(115200);
  // Setup Pins and Variables
  setupPinsAndVars();
  //Connect to the WiFi network
  connectToWiFi(networkName, networkPswd);
  // Attach the interupt
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
}

void loop(){
  // Process sensor info
  String val = waterFlowRateParse();
    if(connected){
      //Send a packet
      udp.beginPacket(udpAddress,udpPort);
      udp.print(val);
      udp.endPacket();
  }
  //Wait for 1 second
  delay(1000);
}

void connectToWiFi(const char * ssid, const char * pwd){
  Serial.println("Connecting to WiFi network: " + String(ssid));

  // delete old config
  WiFi.disconnect(true);
  //register event handler
  WiFi.onEvent(WiFiEvent);
  
  //Initiate connection
  WiFi.begin(ssid, pwd);

  Serial.println("Waiting for WIFI connection...");
}

//wifi event handler
void WiFiEvent(WiFiEvent_t event){
    switch(event) {
      case SYSTEM_EVENT_STA_GOT_IP:
          //When connected set 
          Serial.print("WiFi connected! IP address: ");
          Serial.println(WiFi.localIP());  
          //initializes the UDP state
          //This initializes the transfer buffer
          udp.begin(WiFi.localIP(),udpPort);
          connected = true;
          break;
      case SYSTEM_EVENT_STA_DISCONNECTED:
          Serial.println("WiFi lost connection");
          connected = false;
          break;
      default: break;
    }
}

String waterFlowRateParse(){
      // WATER SENSOR CODE
     if((millis() - oldTime) > 1000)    // Only process counters once per second
  {
    // Disable the interrupt while calculating flow rate and sending the value to
    // the host
    detachInterrupt(sensorInterrupt);    
    // Because this loop may not complete in exactly 1 second intervals we calculate
    // the number of milliseconds that have passed since the last execution and use
    // that to scale the output. We also apply the calibrationFactor to scale the output
    // based on the number of pulses per second per units of measure (litres/minute in
    // this case) coming from the sensor.
    flowRateRaw = ((1000.0 / (millis() - oldTime)) * pulseCount); // No Calibration Factor Applied
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
    // Note the time this processing pass was executed. Note that because we've
    // disabled interrupts the millis() function won't actually be incrementing right
    // at this point, but it will still return the value it was set to just before
    // interrupts went away.
    oldTime = millis();
    // Divide the flow rate in litres/minute by 60 to determine how many litres have
    // passed through the sensor in this 1 second interval, then multiply by 1000 to
    // convert to millilitres.
    flowMilliLitres = (flowRate / 60) * 1000;
    flowMilliLitresRaw = (flowRateRaw / 60) * 1000;
    // Add the millilitres passed in this second to the cumulative total
    totalMilliLitres += flowMilliLitres; 
    unsigned int frac;
    // Print the flow rate for this second in litres / minute
    Serial.print("Flow rate: ");
    Serial.print(int(flowRate));  // Print the integer part of the variable
    Serial.print("L/min");
    Serial.print("\t");       // Print tab space
    // Print the cumulative total of litres flowed since starting
    Serial.print("Output Liquid Quantity: ");        
    Serial.print(totalMilliLitres);
    Serial.print("mL"); 
    Serial.print(" or ");       
    Serial.print(totalMilliLitres/1000);
    Serial.println("L");
    // Reset the pulse counter so we can start incrementing again
    pulseCount = 0;
    String strFlowMilliLitresRaw = String(flowMilliLitresRaw);
    String strFlowRateRaw = String(flowRateRaw);
    String val = String(strFlowMilliLitresRaw + "," + strFlowRateRaw);
    // Enable the interrupt again now that we've finished sending output
    attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
    return val;
  }
}

// Interupt Routine
void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}

void setupPinsAndVars(){
    // Setup PINS and initialise variables
    pinMode(sensorPin, INPUT);
    digitalWrite(sensorPin, HIGH);
    pulseCount        = 0;
    flowRateRaw       = 0.0;
    flowRate          = 0.0;
    flowMilliLitres   = 0;
    totalMilliLitres  = 0;
    oldTime           = 0;
}
