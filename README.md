# Caravan Management Studio

Caravan Management Studio is a system that facilitates monitoring of power, levelling, solar and water on the caravan via means of a web interface that can be viewed via any device connected to the system.

The system is centralised around the Raspberry Pi SBC with additional peripherals added as required to capture additional inputs and outputs.
Currently the system supports the following features:
*  Power Monitoring (Vehicle, Solar, House Battery, Consumption)
*  Water Monitoring (Realtime usage and % remaining)
*  Leveller (Displays Pitch and Roll of van to assist with site setup)
*  History & Daily Metrics (Graphs, running totals etc)

A quick tour of the Application Interface is shown below.
![](Misc/Interface_Overview.MP4)

### Major Components
1.  Raspberry Pi 3B or newer: https://core-electronics.com.au/raspberry-pi-4-model-b-2gb.html
2.  DCDC Solar Charge Controller: https://au.renogy.com/renogy-50a-dc-dc-on-board-battery-charger-with-mppt/
3.  AC Charger: https://www.projecta.com.au/battery-charger-products/12v-automatic-50a-7-stage-battery-charger
4.  RS485 DC Consumption Meter (Qty 2): https://ebay.us/eS8xCk
5.  1 Wire Temp Sensor: https://core-electronics.com.au/high-temp-waterproof-ds18b20-digital-temperature-sensor-extras.html
6.  3DOF Accelerometer & Gyro: https://core-electronics.com.au/mpu-6050-module-3-axis-gyroscope-acce-lerometer.html
7.  Pi Wide Input SHIM: https://core-electronics.com.au/pimoroni-wide-input-shim-kit.html
8.  ESP32: https://core-electronics.com.au/adafruit-huzzah32-esp32-feather-board.html
9.  Water Flow Rate Sensor: https://core-electronics.com.au/g1-2-water-flow-sensor-enclosure-seeed-studio.html
10.  Other additional bolts, cable ties, screws, cable etc

### Block Diagram
![alt text](Diagrams/Block_Diagram.jpg "Block Diagram")

### 3D Models
3D printable enclosures have been designed to house the central Raspberry Pi (with its required peripherals) as well as the external ESP32.
These models can be found in the [Models](/Models) folder.
![alt text](Models/ESP32_Enclosure.png "ESP32 Enclosure") ![alt text](Models/Pi_Enclosure.png "Pi Enclosure")


### Software Diagram
[![](https://mermaid.ink/img/eyJjb2RlIjoiY2xhc3NEaWFncmFtXG4gIFBpICA8fC0tIEVTUDMyXG4gIFBpIDogKyBXSUZJIEhvdHNwb3Rcblx0UGkgOiArIE5vZGUgSlNcblx0UGkgOiArIE5vZGUgUmVkXG4gIFBpIDogKyBVRFAgU2VydmVyXG5cbiAgRVNQMzI6ICsgQysrXG4gIEVTUDMyOiArIFdJRkkgQ2xpZW50XG4gIEVTUDMyOiArIFVEUCBDbGllbnRcblx0XG5cblx0XHRcdFx0XHQiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGFyayJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiY2xhc3NEaWFncmFtXG4gIFBpICA8fC0tIEVTUDMyXG4gIFBpIDogKyBXSUZJIEhvdHNwb3Rcblx0UGkgOiArIE5vZGUgSlNcblx0UGkgOiArIE5vZGUgUmVkXG4gIFBpIDogKyBVRFAgU2VydmVyXG5cbiAgRVNQMzI6ICsgQysrXG4gIEVTUDMyOiArIFdJRkkgQ2xpZW50XG4gIEVTUDMyOiArIFVEUCBDbGllbnRcblx0XG5cblx0XHRcdFx0XHQiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGFyayJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)

### Software Setup and Configuration